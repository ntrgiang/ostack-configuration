#!/usr/bin/env bash

HOME_DIR=/home/giang
OSTACK_DIR=$HOME_DIR/devstack
IMAGE_DIR=$HOME_DIR/os-images
#source $TOP_DIR/env.ini # mine
#
#source $TOP_DIR/functions 
#source $TOP_DIR/stackrc

source $OSTACK_DIR/openrc demo demo

KEY_NAME="hp-ostack"

CIRROS_KERNEL_NAME="cirros-kernel"
CIRROS_RAM_NAME="cirros-ramdisk"

#UBUNTU_IMAGE="ubuntu-14.04-server-cloudimg-i386-disk1"
UBUNTU_IMAGE="kodo-base"

# - - - - -  - - - - - 
# nova
#

# allow ssh-access to VMs
nova 	--os-username=$OS_USERNAME \
	--os-password=secret \
	--os-project-name=$OS_PROJECT_NAME \
	--os-auth-url=$OS_AUTH_URL \
	keypair-add --pub-key $OSTACK_DIR/$KEY_NAME.key.pub $KEY_NAME

nova secgroup-add-rule default tcp 22 22 0.0.0.0/0
nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0

# - - - - - - - - - - -
# glance
#
glance image-create --name $UBUNTU_IMAGE \
	--container-format bare \
	--disk-format qcow2 \
	--file $IMAGE_DIR/$UBUNTU_IMAGE.img \
	--visibility public \
	--min-disk 4 \
	--min-ram 512 \
	--progress

